<?php
class Balticode_Multisiuntos_Model_Observer
{
    public function addMultisiuntosAction($observer)
    {   
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('multisiuntosxml', array(
                'label' => 'Generate Multishipping XML',
                'url' => Mage::app()->getStore()->getUrl('balticode_multisiuntos/adminhtml_xml/generate'),
            ));
        }
    }
    
    public function preDispatch(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $feedModel  = Mage::getModel('Balticode_Multisiuntos_Model_Feed');
            $feedModel->checkUpdate();
        }
    }
}