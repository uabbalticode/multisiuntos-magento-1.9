<?php

class Balticode_Multisiuntos_Model_Data {
	private $cdata = false;

	public function getDecimal($number, $precision, $scale){
		return number_format($number,$scale,".","");
	}
	public function getComment($order){
		$enabled_comments = Mage::getStoreConfig('shipping/multisiuntos/comment');
		if($enabled_comments){
			$all_comments = $order->getStatusHistoryCollection()->getData();
			$comment = $all_comments[0]['comment'];
			if($comment == "") $comment = null;
			else if($this->cdata) $comment = array("@cdata" => $comment);
		} else {
			$comment = null;
		}
		return $comment;
	}
}

?>