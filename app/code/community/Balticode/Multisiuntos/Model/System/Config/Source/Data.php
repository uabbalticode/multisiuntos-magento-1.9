<?php
class Balticode_Multisiuntos_Model_System_Config_Source_Data
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
			array('value' => 'OMNIVA_LT', 'label' => Mage::helper('adminhtml')->__('OMNIVA_LT')),
			array('value' => 'OMNIVA_LV', 'label' => Mage::helper('adminhtml')->__('OMNIVA_LV')),
			array('value' => 'OMNIVA_EE', 'label' => Mage::helper('adminhtml')->__('OMNIVA_EE')),
			array('value' => 'LP_EXPRESS', 'label' => Mage::helper('adminhtml')->__('LP_EXPRESS')),
			array('value' => 'VENIPAK', 'label' => Mage::helper('adminhtml')->__('VENIPAK')),
			array('value' => 'DPD_LT', 'label' => Mage::helper('adminhtml')->__('DPD_LT')),
			array('value' => 'DPD_LV', 'label' => Mage::helper('adminhtml')->__('DPD_LV')),
			array('value' => 'DPD_EE', 'label' => Mage::helper('adminhtml')->__('DPD_EE')),
			array('value' => 'UPS', 'label' => Mage::helper('adminhtml')->__('UPS')),
			array('value' => 'UPS_LT', 'label' => Mage::helper('adminhtml')->__('UPS_LT')),
			array('value' => 'UPS_LV', 'label' => Mage::helper('adminhtml')->__('UPS_LV')),
			array('value' => 'UPS_EE', 'label' => Mage::helper('adminhtml')->__('UPS_EE')),
			array('value' => 'TNT', 'label' => Mage::helper('adminhtml')->__('TNT')),
			array('value' => 'TNT_LT', 'label' => Mage::helper('adminhtml')->__('TNT_LT')),
			array('value' => 'TNT_LV', 'label' => Mage::helper('adminhtml')->__('TNT_LV')),
			array('value' => 'TNT_EE', 'label' => Mage::helper('adminhtml')->__('TNT_EE')),
			array('value' => 'DHL', 'label' => Mage::helper('adminhtml')->__('DHL')),
			array('value' => 'DHL_LT', 'label' => Mage::helper('adminhtml')->__('DHL_LT')),
			array('value' => 'DHL_LV', 'label' => Mage::helper('adminhtml')->__('DHL_LV')),
			array('value' => 'DHL_EE', 'label' => Mage::helper('adminhtml')->__('DHL_EE')),
			array('value' => 'LT_POST', 'label' => Mage::helper('adminhtml')->__('LT_POST')),
			array('value' => 'LV_POST', 'label' => Mage::helper('adminhtml')->__('LV_POST')),
			array('value' => 'ITELLA', 'label' => Mage::helper('adminhtml')->__('ITELLA')),
			array('value' => 'SMARTBUS', 'label' => Mage::helper('adminhtml')->__('SMARTBUS')),
        );
    }

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
	    return array(
			'OMNIVA_LT' => Mage::helper('adminhtml')->__('OMNIVA_LT'),
			'OMNIVA_LV' => Mage::helper('adminhtml')->__('OMNIVA_LV'),
			'OMNIVA_EE' => Mage::helper('adminhtml')->__('OMNIVA_EE'),
			'LP_EXPRESS' => Mage::helper('adminhtml')->__('LP_EXPRESS'),
			'VENIPAK' => Mage::helper('adminhtml')->__('VENIPAK'),
			'DPD_LT' => Mage::helper('adminhtml')->__('DPD_LT'),
			'DPD_LV' => Mage::helper('adminhtml')->__('DPD_LV'),
			'DPD_EE' => Mage::helper('adminhtml')->__('DPD_EE'),
			'UPS' => Mage::helper('adminhtml')->__('UPS'),
			'UPS_LT' => Mage::helper('adminhtml')->__('UPS_LT'),
			'UPS_LV' => Mage::helper('adminhtml')->__('UPS_LV'),
			'UPS_EE' => Mage::helper('adminhtml')->__('UPS_EE'),
			'TNT' => Mage::helper('adminhtml')->__('TNT'),
			'TNT_LT' => Mage::helper('adminhtml')->__('TNT_LT'),
			'TNT_LV' => Mage::helper('adminhtml')->__('TNT_LV'),
			'TNT_EE' => Mage::helper('adminhtml')->__('TNT_EE'),
			'DHL' => Mage::helper('adminhtml')->__('DHL'),
			'DHL_LT' => Mage::helper('adminhtml')->__('DHL_LT'),
			'DHL_LV' => Mage::helper('adminhtml')->__('DHL_LV'),
			'DHL_EE' => Mage::helper('adminhtml')->__('DHL_EE'),
			'LT_POST' => Mage::helper('adminhtml')->__('LT_POST'),
			'LV_POST' => Mage::helper('adminhtml')->__('LV_POST'),
			'ITELLA' => Mage::helper('adminhtml')->__('ITELLA'),
			'SMARTBUS' => Mage::helper('adminhtml')->__('SMARTBUS'),
	    );
	}
}
?>