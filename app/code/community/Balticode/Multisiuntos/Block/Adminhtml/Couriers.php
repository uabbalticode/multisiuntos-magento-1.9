<?php

class Balticode_Multisiuntos_Block_Adminhtml_Couriers extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected $_addRowButtonHtml = array();
    protected $_removeRowButtonHtml = array();

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $html = '<div class="grid" >';
        $html .= '<table style="display:none">';
        $html .= '<tbody id="multisiuntos_template">';
        $html .= $this->_getRowTemplateHtml();
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table class="border" cellspacing="0" cellpadding="0">';
        $html .= '<tbody id="multisiuntos_container">';
        $html .= '<tr class="headings">';
        $html .= '<th>'.$this->__('Shipping method').'</th><th>'.$this->__('Courier identifier').'</th><th>'
                .$this->__('Express').'</th><th>'.$this->__('Pickup is required').'</th><th>'
                .$this->__('Loading is required').'</th><th>'.$this->__('Saturday').'</th><th>'
                .$this->__('Insurance').'</th><th>'.$this->__('Proof').'</th><th>&nbsp;</th>';
        $html .= '</tr>';
        if ($this->_getValue('shipping_method')) {
            foreach ($this->_getValue('shipping_method') as $i=>$f) {
                if ($i) {
                    $html .= $this->_getRowTemplateHtml($i);
                }
            }
        }
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= $this->_getAddRowButtonHtml('multisiuntos_container', 'multisiuntos_template', $this->__('Add combination'));

        return $html;
    }

    protected function _getRowTemplateHtml($i=0)
    {
        $objects = Mage::getModel('multisiuntos/system_config_source_data')->toOptionArray();
        $shipping_methods = Mage::helper('multisiuntos')->getShipingMethodOptions();

        $html = '<tr>';
        $html .= '<td>';
        $html .= '<select id="multisiuntos_shipping" class="option-control" style="width: 120px" value="" name="'
                    .$this->getElement()->getName().'[shipping_method][]" >';
        $html .= '<option value="__empty">'.$this->__('Select shipping method').'</option>';
        foreach($shipping_methods as $shipping_method)
        {
            if($shipping_method['value'] == $this->_getValue('shipping_method/'.$i))
                $html .= '<option value="'.$shipping_method['value'].'" selected>'.$shipping_method['label'].'</option>';
            else
                $html .= '<option value="'.$shipping_method['value'].'">'.$shipping_method['label'].'</option>';
        }
        $html .= "</select>";
        $html .= '</td>';
        $html .= '<td>';
        $html .= '<select id="multisiuntos_courier" class="option-control" style="width: 120px" value="" name="'
                    .$this->getElement()->getName().'[courier_method][]" >';
        $html .= '<option value="__empty">'.$this->__('Select courier identifier').'</option>';
        foreach ($objects as $key => $courier) {
            if($courier['value'] == $this->_getValue('courier_method/'.$i))
                $html .= '<option value="'.$courier['value'].'" selected>'.$courier['label'].'</option>';
            else
                 $html .= '<option value="'.$courier['value'].'">'.$courier['label'].'</option>';
        }
        $html .= "</select>";
        $html .= '</td>';
        $list = array('express','self','loading','saturday','insurance','proof');
        foreach ($list as $name) {
            $html .= '<td>'.$this->_getSelectOptionsNoYes($name,$i).'</td>';
        }
        $html .= '<td>'.$this->_getRemoveRowButtonHtml().'</td>';
        $html .= '</tr>';

        return $html;
    }

    protected function _getSelectOptionsNoYes($name, $i){
        $html = '<select id="multisiuntos_'.$name.'" class="option-control" style="width: 50px" value="" name="'
                .$this->getElement()->getName().'['.$name.'][]" >';
        $html .= '<option value="0" '.(($this->_getValue($name.'/'.$i)==0)?"selected":"").'>'.__('No').'</option>';
        $html .= '<option value="1" '.(($this->_getValue($name.'/'.$i)==1)?"selected":"").'>'.__('Yes').'</option>';
        $html .= '</select>';
        return $html;
    }

    protected function _getDisabled()
    {
        return $this->getElement()->getDisabled() ? ' disabled' : '';
    }

    protected function _getValue($key)
    {
        return $this->getElement()->getData('value/'.$key);
    }

    protected function _getAddRowButtonHtml($container, $template, $title='Add')
    {
        if (!isset($this->_addRowButtonHtml[$container])) {
            $this->_addRowButtonHtml[$container] = $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setType('button')
                ->setClass('add '.$this->_getDisabled())
                ->setLabel($this->__($title))
                ->setOnClick("Element.insert($('".$container."'), {bottom: $('".$template."').innerHTML})")
                ->setDisabled($this->_getDisabled())
                ->toHtml();
        }
        return $this->_addRowButtonHtml[$container];
    }

    protected function _getRemoveRowButtonHtml($selector='tr', $title='Delete')
    {
        if (!$this->_removeRowButtonHtml) {
            $this->_removeRowButtonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setType('button')
                ->setClass('delete v-middle '.$this->_getDisabled())
                ->setLabel($this->__($title))
                ->setOnClick("Element.remove($(this).up('".$selector."'))")
                ->setDisabled($this->_getDisabled())
                ->toHtml();
        }
        return $this->_removeRowButtonHtml;
    }
}
