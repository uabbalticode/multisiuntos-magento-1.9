<?php

class Balticode_Multisiuntos_Helper_Data extends Mage_Core_Helper_Abstract{

    private $express_available = array(
        'TNT',
        'TNT_LT',
        'TNT_LV',
        'TNT_EE',
        'DHL',
        'DHL_LT',
        'DHL_LV',
        'DHL_EE',
        'UPS_LT',
        'UPS_LV',
        'UPS_EE',
    );
    private $cod_methods = array('cashondelivery');
    private $shipping_methods_to_get_parcel_stores = "balticode,post24lithuania,dpd_ps";

	public function getShipingMethodOptions($storeId = null)
	{
		$carriers = Mage::getSingleton('shipping/config')->getAllCarriers($storeId);
		$options = array();
		foreach($carriers as $carrierCode=>$carrierConfig)
		{
            if(version_compare(Mage::getVersion(), '1.5.0', '>='))
            {
                $methods = $carrierConfig->getAllowedMethods();

                if($methods)
                {
                    if(count($methods) > 1)
                    {
                        foreach($methods as $method=>$methodName)
                        {
                            if(!is_array($methodName))
                            {
                                if(preg_match('#^([a-zA-Z0-9])+$#', $method))
                                {
                                    array_unshift($options, array(
                                        'value' => $carrierCode.'_'.$method,
                                        'label' => $this->getCarrierName($carrierCode) . ' - ' .$methodName
                                    ));
                                }
                            }
                        }
                    }
                }
            }

            array_unshift($options, array(
                'value' => $carrierCode.'_'.$carrierCode,
                'label' => $this->getCarrierName($carrierCode)
            ));

		}
		return $options;
	}
	
	public function getCarrierName($carrierCode)
    {
        if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title')) {
            return $name;
        }
        return $carrierCode;
    }

    private function getCourierIdentifierIdByShippingMethod($shipping_method){
        if( $name = Mage::getStoreConfig('shipping/multisiuntos/multisiuntos') ){
            $shipping_methods_config = unserialize( Mage::getStoreConfig('shipping/multisiuntos/multisiuntos') );
            $shipping_methods_array = array_flip( $shipping_methods_config['shipping_method'] );
            if(isset($shipping_methods_array[$shipping_method])){
                return $shipping_methods_array[$shipping_method];
            }
        }
        return false;
    }

    public function getCourierIdentifierByShippingMethod($shipping_method){
        $id = $this->getCourierIdentifierIdByShippingMethod($shipping_method);
        if($id){
            $shipping_methods_config = unserialize( Mage::getStoreConfig('shipping/multisiuntos/multisiuntos') );
            return $shipping_methods_config['courier_method'][$id];
        }
        return $shipping_method;
    }

    public function getCourierAttributeByShippingMethod($shipping_method,$attributeName){
        if($attributeName == 'express'){ //If atrribute is express, nead to test it is TNT UPS or DHL courier
            if(!in_array($this->getCourierIdentifierByShippingMethod($shipping_method), $this->express_available)){
                return false;
            }
        }
        $id = $this->getCourierIdentifierIdByShippingMethod($shipping_method);
        if ($id) { //If found id return value   
            $shipping_methods_config = unserialize( Mage::getStoreConfig('shipping/multisiuntos/multisiuntos') );
            if (isset($shipping_methods_config[$attributeName][$id])) {
                return $shipping_methods_config[$attributeName][$id]; //return value from config
            } else {
                Mage::Log('Courier Identifier Id is found: `'.$id.'`, but attribute: `'.$attributeName.'` not found! This is all what i heave: '.print_r($shipping_methods_config,true),null,'MultishippingXML.log');
                return false;
            }
        } else {
            return false; //return false
        }

    }

    public function getPayment($order){
        $codArray = array();
        if(in_array($order->getPayment()->getData('method'),$this->cod_methods)){ //if COD grab info
            $codArray = array("value" => (float)Mage::getModel('multisiuntos/data')->getDecimal($order->getData('grand_total'),8,2), //Siuntinio suma
                 "reference" => null, //Klienu ir sistemos vidinis COD suma
                 "currency" => $order->getData('order_currency_code') //Valiuta
                 );
        }
        return $codArray;
    }

    public function getParcelStoreId($order)
    {
        $parcel_store_id = null;
        $shipping_methods = explode(",", $this->shipping_methods_to_get_parcel_stores);
        foreach ($shipping_methods as $shipping_method) {
            if (strstr($order->getData('shipping_method'), $shipping_method) !== false) { //If found association
                if ($shipping_method == 'post24lithuania') { //If Omniva so grab ID from name
                    preg_match("/(\d+)$/",$order->getData('shipping_method'), $match);
                    $parcel_store_id = $match[0];
                }
                if ($shipping_method == 'balticode') { //If Balticode so grab ID from name
                    preg_match("/(\d+)$/",$order->getData('shipping_method'), $match);
                    $parcel_store_id = $match[0];
                }
                if ($shipping_method == 'dpd_ps') {
                    $dpd = Mage::getModel('dpd/carriers_parcelstore_parcelstore');
                    if (method_exists($dpd, 'getParcelStoreIdByOrder')) {
                        $parcel_store_id = $dpd->getParcelStoreIdByOrder($order->getEntityId());
                    }
                }
            }
        }

        return $parcel_store_id;
    }

}
