<?php

class Balticode_Multisiuntos_Adminhtml_XmlController
    extends Mage_Adminhtml_Controller_Action
{
    public $filename = 'customers_orders.xml';

    public function generateAction() {
        $orderIds = $this->getRequest()->getPost('order_ids');
        if (!isset($orderIds)) {
            return false; //If something wrong
        }
        $data = Mage::getModel('multisiuntos/data');
        $helper = Mage::helper("multisiuntos/data");

        $orders_array = array(
            '@attributes' => array(
                'version' => '1'
            ),
            'shipment' => array(
            )
        );
        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $order_data = $order->getData();
            $shipping_data = $order->getShippingAddress()->getData();

            $orders_array['shipment'][] = array(
            "reference" => $order_data['entity_id'], //Orderio Id
            "weight" => (float)$data->getDecimal($order_data['weight'],8,3), //Svoris kilogramais
            "remark" => $data->getComment($order), //Papildoma informacija, kuri eina ant lipduko
            "additional_information" => null, //Papildoma informacija
            //"number_of_parcels" => (int)$order['total_qty_ordered'], //Pakuociu skaicius per uzsakyma
            "number_of_parcels" => 1,
            "courier_identifier" => $helper->getCourierIdentifierByShippingMethod($order_data['shipping_method']), //Kurjerio identifikatorius
            "receiver" => array( //Gavejo info
                    "name" => $shipping_data['firstname'], //Vardas
                    "street" => $shipping_data['street'], //Gatve
                    "postal_code" => $shipping_data['postcode'], //Pasto kodas
                    "city" => $shipping_data['city'], //Miestas
                    "phone" => $shipping_data['telephone'], //Telefono numeris
                    "email" => $order_data['customer_email'], //Elektroninio pasto adresas
                    "parcel_terminal_identifier" => $helper->getParcelStoreId($order), //Siuntu tasko ID
                    "country_code" => $shipping_data['country_id'] //Salies kodas
                ),
            "services" => array( //Vezejo info
                "cash_on_delivery" => $helper->getPayment($order), //Gauname COD duomenis
                "express_delivery" => (bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'express' ), //Ar tai Express pristatymas?
                "self_service" => (bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'self' ), //Ar reikalingas pakrovimas
                "loading_service" => (bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'loading' ), //Pakrovimo servisas
                "saturday_delivery" => (bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'saturday' ), //Savaitgalinis vezimas
                "insurance" => (bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'insurance' ), //Draudimas
                "proof_of_delivery" => (bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'proof' ) //Pristatymo patvirinimas
                )
            );
        }

        $array2xml = Mage::getModel('multisiuntos/array2xml');
        $xml = $array2xml->createXML('shipments', $orders_array);
        $this->_prepareDownloadResponse($this->filename, $xml->saveXML());
    }

    protected function _isAllowed()
    {
       return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/multisiuntos');
    }
}
